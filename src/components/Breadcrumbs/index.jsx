import React from 'react';
import cx from 'classnames';
import { Link, withRouter } from 'react-router-dom';
import style from './style.module.css';

const Breadcrumbs = ({ location }) => {
  const pathFragments = location.pathname.split('/').filter(Boolean);
  return (
    <nav aria-label="breadcrumb">
      <ol className="breadcrumb">
        {pathFragments.map((fragment, index) => {
          const isFirstBreadcrumb = index === 0;
          const isLastBreadcrumb = index + 1 === pathFragments.length;
          const breadcrumbText = isFirstBreadcrumb ? 'Disk' : fragment;
          const breadcrumbElement = isLastBreadcrumb ? (
            breadcrumbText
          ) : (
            <Link to={`/${pathFragments.slice(0, index + 1).join('/')}`}>
              {breadcrumbText}
            </Link>
          );
          return (
            <li
              className={cx(
                'breadcrumb-item',
                style.breadcrumb,
                isFirstBreadcrumb && 'font-weight-bold text-primary',
                isLastBreadcrumb && 'active'
              )}
            >
              {breadcrumbElement}
            </li>
          );
        })}
      </ol>
    </nav>
  );
};

export default withRouter(Breadcrumbs);
