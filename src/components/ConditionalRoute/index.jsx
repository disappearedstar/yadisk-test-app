import React from 'react';
import { Route, Redirect } from 'react-router-dom';

const ConditionalRoute = ({
  component: Component,
  condition,
  redirectTo,
  ...rest
}) => (
  <Route
    {...rest}
    render={props =>
      condition ? (
        <Component {...props} />
      ) : (
        <Redirect
          to={{
            pathname: redirectTo,
            state: { from: props.location },
          }}
        />
      )
    }
  />
);

export default ConditionalRoute;
