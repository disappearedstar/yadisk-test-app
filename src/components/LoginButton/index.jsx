import React, { useCallback } from 'react';
import { navigateTo } from '../../utils/navigation';
import config from '../../config';

const OAUTH_AUTHRORIZE_ENDPOINT = `https://oauth.yandex.ru/authorize?response_type=token&client_id=${config.API_CLIENT_ID}`;

const LoginButton = () => {
  const onLogin = useCallback(() => {
    navigateTo(OAUTH_AUTHRORIZE_ENDPOINT);
  }, []);

  return (
    <button className="btn btn-primary btn-lg" onClick={onLogin}>
      Sign in via Yandex
    </button>
  );
};

export default LoginButton;
