import React from 'react';
import { IconFile, IconFolder } from '../Icon';
import { humanizeFileSize, createResourceNameStripper } from '../../utils/text';
import style from './style.module.css';

const RESOURCE_NAME_LENGTH_MAX = 50;
const RESOURCE_THUMBNAIL_SIZE = 56;

const stripResourceName = createResourceNameStripper(RESOURCE_NAME_LENGTH_MAX);

export const DiskResourceFolder = ({
  resource,
  size = RESOURCE_THUMBNAIL_SIZE,
}) => (
  <div className={style.container}>
    <IconFolder size={size} />
    <span title={resource.name} className={style.resourceName}>
      {stripResourceName(resource.name)}
    </span>
  </div>
);

export const DiskResourceFile = ({
  resource,
  size = RESOURCE_THUMBNAIL_SIZE,
}) => (
  <div className={style.container}>
    {resource.preview ? (
      <img
        className={style.resourceThumbnail}
        alt=""
        src={resource.preview}
        style={{ width: size, height: size, minWidth: size }}
      />
    ) : (
      <IconFile size={size} />
    )}
    <span title={resource.name} className={style.resourceName}>
      {stripResourceName(resource.name)}
    </span>
    <span className={style.resourceSize}>
      {humanizeFileSize(resource.size)}
    </span>
  </div>
);
