import React from 'react';
import { useSelector } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import {
  DiskResourceFile,
  DiskResourceFolder,
} from '../../components/DiskResource';
import Spinner from '../../components/Spinner';
import {
  isFetchingResourcesSelector,
  isResourcesNotFoundSelector,
} from '../../modules/resources';
import { trimTrailingSlash } from '../../utils/text';
import routes from '../../routes';
import style from './style.module.css';

const DiskContent = ({ location, resources }) => {
  const isFetchingResources = useSelector(isFetchingResourcesSelector);
  const isResourcesNotFound = useSelector(isResourcesNotFoundSelector);

  if ((!resources && !isResourcesNotFound) || isFetchingResources) {
    return <Spinner />;
  }
  if (isResourcesNotFound) {
    return (
      <div>
        Not found. <Link to={routes.DISK}>Return to root</Link>
      </div>
    );
  }
  if (resources.length === 0) {
    return <div>Empty</div>;
  }

  return (
    <React.Fragment>
      {resources
        .filter(({ type }) => type === 'dir')
        .map(resource => (
          <Link
            to={`${trimTrailingSlash(location.pathname)}/${resource.name}`}
            className={style.link}
          >
            <DiskResourceFolder
              key={resource.resource_id}
              resource={resource}
            />
          </Link>
        ))}
      {resources
        .filter(({ type }) => type === 'file')
        .map(resource => (
          <DiskResourceFile key={resource.resource_id} resource={resource} />
        ))}
    </React.Fragment>
  );
};

export default withRouter(DiskContent);
