import React from 'react';
import cx from 'classnames';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import LogoutButton from '../LogoutButton';
import {
  userDisplayNameSelector,
  isAuthenticatedSelector,
} from '../../modules/profile';
import routes from '../../routes';
import style from './style.module.css';

const Navbar = () => {
  const displayName = useSelector(userDisplayNameSelector);
  const isAuthenticated = useSelector(isAuthenticatedSelector);
  return (
    <nav
      className={cx(
        style.navbar,
        'navbar navbar-dark bg-dark position-fixed w-100 flex-nowrap'
      )}
    >
      <Link to={routes.INDEX} className="navbar-brand text-primary" href="#">
        Yadisk Test App
      </Link>
      {isAuthenticated && (
        <div className="d-flex flex-nowrap">
          <span className="text-light text-nowrap mx-3">{displayName}</span>
          <LogoutButton />
        </div>
      )}
    </nav>
  );
};

export default Navbar;
