import React from 'react';
import cx from 'classnames';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import LoginButton from '../LoginButton';
import { isAuthenticatedSelector } from '../../modules/profile';
import routes from '../../routes';
import style from './style.module.css';

const IndexScreen = () => {
  const isAuthenticated = useSelector(isAuthenticatedSelector);
  return (
    <div className="jumbotron bg-transparent text-white position-relative mx-auto d-flex flex-column flex-grow-1 align-items-center justify-content-center">
      <h1 className="display-4">Yadisk Test App</h1>
      <p className={cx('lead', style.text)}>
        Connect your Yandex account and see the list of files and folders on
        your Yandex.Disk right in your browser! Sounds like a magic, isn't it?
      </p>
      {isAuthenticated ? (
        <Link to={routes.DISK}>
          <button className="btn btn-primary btn-lg">Go to Disk</button>
        </Link>
      ) : (
        <LoginButton />
      )}
    </div>
  );
};

export default IndexScreen;
