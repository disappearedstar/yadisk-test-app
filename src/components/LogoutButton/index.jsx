import React, { useCallback } from 'react';
import { deleteAccessToken } from '../../utils/auth';
import { navigateTo } from '../../utils/navigation';
import routes from '../../routes';

const LogoutButton = () => {
  const onLogout = useCallback(() => {
    deleteAccessToken();
    navigateTo(routes.INDEX);
  }, []);

  return (
    <button
      type="button"
      className="btn btn-outline-danger btn-sm text-nowrap"
      onClick={onLogout}
    >
      Sign out
    </button>
  );
};

export default LogoutButton;
