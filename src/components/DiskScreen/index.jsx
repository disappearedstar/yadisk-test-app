import React from 'react';
import DiskContainer from '../../containers/DiskContainer';
import Breadcrumbs from '../Breadcrumbs';

const DiskScreen = () => {
  return (
    <div className="p-3 m-4 bg-white flex-grow-1 position-relative rounded">
      <Breadcrumbs />
      <div>
        <DiskContainer />
      </div>
    </div>
  );
};

export default DiskScreen;
