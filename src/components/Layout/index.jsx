import React from 'react';
import cx from 'classnames';
import Navbar from '../../components/Navbar';
import style from './style.module.css';

const Layout = ({ children }) => (
  <div className={cx(style.layout, 'd-flex flex-column flex-grow-1')}>
    <Navbar />
    <div className={cx(style.cover, 'd-flex flex-column flex-grow-1')}>
      {children}
    </div>
  </div>
);

export default Layout;
