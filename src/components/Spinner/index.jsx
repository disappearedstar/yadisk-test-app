import React from 'react';
import style from './style.module.css';

const Spinner = () => (
  <div className={style.container}>
    <div className="spinner-border text-primary">
      <span className="sr-only" />
    </div>
  </div>
);

export default Spinner;
