import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import rootReducer from '../modules';

const middleware = [thunkMiddleware];
if (process.env.NODE_ENV === 'development') {
  middleware.push(
    require('redux-logger').createLogger({
      collapsed: true,
    })
  );
}

export default function configureStore(initialState = {}) {
  const enhancer = applyMiddleware(...middleware);
  const store = createStore(rootReducer, initialState, enhancer);

  if (module.hot) {
    module.hot.accept('../modules', () => {
      store.replaceReducer(rootReducer);
    });
  }

  return store;
}
