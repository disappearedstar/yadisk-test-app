import React from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { fetchResources, resourcesDataSelector } from '../../modules/resources';
import DiskContent from '../../components/DiskContent';

const buildResourcesPath = path => `disk:/${path || ''}`;

class DiskContainer extends React.Component {
  componentDidMount() {
    this.fetchResources();
  }

  componentDidUpdate(prevProps) {
    if (
      prevProps.location.pathname !== this.props.location.pathname &&
      !this.props.resources
    ) {
      this.fetchResources();
    }
  }

  fetchResources = () => {
    const resourcesPath = buildResourcesPath(this.props.match.params[0]);
    this.props.fetchResources(resourcesPath);
  };

  render() {
    return <DiskContent resources={this.props.resources} />;
  }
}

const mapStateToProps = (state, { match }) => ({
  resources: resourcesDataSelector(state)[buildResourcesPath(match.params[0])],
});

const enhance = compose(
  withRouter,
  connect(
    mapStateToProps,
    { fetchResources }
  )
);

export default enhance(DiskContainer);
