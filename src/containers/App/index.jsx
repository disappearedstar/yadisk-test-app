import React from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { Route, Switch, withRouter } from 'react-router-dom';
import Layout from '../../components/Layout';
import ConditionalRoute from '../../components/ConditionalRoute';
import DiskScreen from '../../components/DiskScreen';
import IndexScreen from '../../components/IndexScreen';
import Spinner from '../../components/Spinner';
import {
  fetchUserProfile,
  hasTriedToAuthSelector,
  isAuthenticatedSelector,
} from '../../modules/profile';
import { extractAccessTokenFromURL } from '../../utils/auth';
import { setupApi } from '../../api/setup';
import routes from '../../routes';

class App extends React.PureComponent {
  componentDidMount() {
    const { history, location } = this.props;
    extractAccessTokenFromURL(() => {
      // Clean hash params from OAuth redirect to avoid issues
      // with unwanted auth attempts when navigating back / forth
      history.replace({
        pathname: location.pathname,
        search: location.search,
      });
    });
    setupApi();
    this.props.fetchUserProfile();
  }

  render() {
    const { hasTriedToAuth, isAuthenticated } = this.props;
    return (
      <Layout>
        {hasTriedToAuth ? (
          <Switch>
            <Route path={routes.INDEX} exact component={IndexScreen} />
            <ConditionalRoute
              path={`${routes.DISK}/*`}
              component={DiskScreen}
              condition={isAuthenticated}
              redirectTo={routes.INDEX}
            />
            <ConditionalRoute
              path={routes.DISK}
              component={DiskScreen}
              condition={isAuthenticated}
              redirectTo={routes.INDEX}
            />
          </Switch>
        ) : (
          <Spinner />
        )}
      </Layout>
    );
  }
}

const mapStateToProps = state => ({
  hasTriedToAuth: hasTriedToAuthSelector(state),
  isAuthenticated: isAuthenticatedSelector(state),
});

const enhance = compose(
  withRouter,
  connect(
    mapStateToProps,
    { fetchUserProfile }
  )
);

export default enhance(App);
