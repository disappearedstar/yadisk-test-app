import byteSize from 'byte-size';

export const humanizeFileSize = bytes =>
  byteSize(bytes, { precision: 0 }).toString();

export const trimTrailingSlash = str => {
  const lastCharIndex = str.length - 1;
  return str[lastCharIndex] === '/' ? str.substring(0, lastCharIndex) : str;
};

export const createResourceNameStripper = length => name =>
  name.length <= length
    ? name
    : // Example: somethi…g.docx
      `${name.substring(0, length - 7)}…${name.substring(name.length - 6)}`;
