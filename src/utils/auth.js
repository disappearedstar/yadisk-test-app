import { parse } from 'query-string';
import config from '../config';
import { LocalStorage } from './storage';

export const retrieveAccessTokenFromStorage = () =>
  LocalStorage.getItem(config.API_ACCESS_TOKEN_STORAGE_KEY);

export const saveAccessToken = accessToken =>
  LocalStorage.setItem(config.API_ACCESS_TOKEN_STORAGE_KEY, accessToken);

export const extractAccessTokenFromURL = cleanupFn => {
  const params = parse(window.location.hash);
  if (params.access_token) {
    saveAccessToken(params.access_token);
    cleanupFn();
  }
};

export const deleteAccessToken = () =>
  LocalStorage.removeItem(config.API_ACCESS_TOKEN_STORAGE_KEY);
