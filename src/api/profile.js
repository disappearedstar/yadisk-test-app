import axios from 'axios';

export const getUserProfile = () =>
  axios.get('/', { params: { fields: 'user' } });
