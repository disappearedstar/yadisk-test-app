import axios from 'axios';
import config from '../config';
import { retrieveAccessTokenFromStorage } from '../utils/auth';

export const setupApi = () => {
  axios.defaults.baseURL = config.API_URL;
  axios.defaults.withCredentials = true;

  const accessToken = retrieveAccessTokenFromStorage();
  if (accessToken) {
    axios.defaults.headers.common['Authorization'] = `OAuth ${accessToken}`;
  }
};
