import axios from 'axios';

export const getDiskResources = path =>
  axios.get('/resources', { params: { path } });
