const config = {
  API_CLIENT_ID: process.env.REACT_APP_API_CLIENT_ID,
  API_URL: process.env.REACT_APP_API_URL,
  API_ACCESS_TOKEN_STORAGE_KEY: 'access_token',
};

export default config;
