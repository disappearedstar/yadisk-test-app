import { getDiskResources } from '../api/resources';

const FETCH_RESOURCES_START = 'app/resources/FETCH_RESOURCES_START';
const FETCH_RESOURCES_SUCCESS = 'app/resources/FETCH_RESOURCES_SUCCESS';
const FETCH_RESOURCES_FAILURE = 'app/resources/FETCH_RESOURCES_FAILURE';
const SET_RESOURCES_NOT_FOUND = 'app/resources/SET_RESOURCES_NOT_FOUND';

const initialState = {
  isFetchingResources: false,
  isResourcesNotFound: false,
  resourcesData: {},
};

const resources = (state = initialState, { type, payload }) => {
  switch (type) {
    case FETCH_RESOURCES_START:
      return {
        ...state,
        isFetchingResources: true,
      };
    case FETCH_RESOURCES_SUCCESS:
      return {
        ...state,
        isFetchingResources: false,
        resourcesData: {
          ...state.resourcesData,
          ...payload,
        },
      };
    case FETCH_RESOURCES_FAILURE:
      return {
        ...state,
        isFetchingResources: false,
      };
    case SET_RESOURCES_NOT_FOUND:
      return {
        ...state,
        isResourcesNotFound: payload,
      };
    default:
      return state;
  }
};

const fetchResourcesStart = () => ({
  type: FETCH_RESOURCES_START,
});

const fetchResourcesSuccess = resourceData => ({
  type: FETCH_RESOURCES_SUCCESS,
  payload: resourceData,
});

const fetchResourcesFailure = () => ({
  type: FETCH_RESOURCES_FAILURE,
});

const setResourcesNotFound = status => ({
  type: SET_RESOURCES_NOT_FOUND,
  payload: status,
});

export const fetchResources = resourcesPath => dispatch => {
  dispatch(setResourcesNotFound(false));
  dispatch(fetchResourcesStart());
  return getDiskResources(resourcesPath).then(
    response =>
      dispatch(
        fetchResourcesSuccess({
          [resourcesPath]: response.data._embedded.items,
        })
      ),
    error => {
      dispatch(fetchResourcesFailure());
      if (
        error.response &&
        error.response.data &&
        error.response.data.error === 'DiskNotFoundError'
      ) {
        dispatch(setResourcesNotFound(true));
      }
      return Promise.reject(error);
    }
  );
};

const getSubstate = state => state.resources;

export const resourcesDataSelector = state => getSubstate(state).resourcesData;
export const isFetchingResourcesSelector = state =>
  getSubstate(state).isFetchingResources;
export const isResourcesNotFoundSelector = state =>
  getSubstate(state).isResourcesNotFound;

export default resources;
