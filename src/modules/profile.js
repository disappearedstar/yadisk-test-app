import { getUserProfile } from '../api/profile';

const FETCH_USER_PROFILE_START = 'app/profile/FETCH_USER_PROFILE_START';
const FETCH_USER_PROFILE_SUCCESS = 'app/profile/FETCH_USER_PROFILE_SUCCESS';
const FETCH_USER_PROFILE_FAILURE = 'app/profile/FETCH_USER_PROFILE_FAILURE';
const SET_AUTHENTICATION_STATUS = 'app/profile/SET_AUTHENTICATION_STATUS';

const initialState = {
  isAuthenticated: undefined,
  displayName: undefined,
  isFetchingUserProfile: false,
};

const profile = (state = initialState, { type, payload }) => {
  switch (type) {
    case FETCH_USER_PROFILE_START:
      return {
        ...state,
        isFetchingUserProfile: true,
      };
    case FETCH_USER_PROFILE_SUCCESS:
      return {
        ...state,
        isFetchingUserProfile: false,
        displayName: payload.display_name,
      };
    case FETCH_USER_PROFILE_FAILURE:
      return {
        ...state,
        isFetchingUserProfile: false,
      };
    case SET_AUTHENTICATION_STATUS:
      return {
        ...state,
        isAuthenticated: payload.isAuthenticated,
      };
    default:
      return state;
  }
};

const fetchUserProfileStart = () => ({
  type: FETCH_USER_PROFILE_START,
});

const fetchUserProfileSuccess = profileData => ({
  type: FETCH_USER_PROFILE_SUCCESS,
  payload: profileData,
});

const fetchUserProfileFailure = () => ({
  type: FETCH_USER_PROFILE_FAILURE,
});

const setAuthenticationStatus = authStatus => ({
  type: SET_AUTHENTICATION_STATUS,
  payload: authStatus,
});

export const fetchUserProfile = () => dispatch => {
  dispatch(fetchUserProfileStart());
  return getUserProfile().then(
    response => {
      dispatch(fetchUserProfileSuccess(response.data.user));
      dispatch(setAuthenticationStatus({ isAuthenticated: true }));
      return response.data.user;
    },
    error => {
      dispatch(fetchUserProfileFailure());
      dispatch(setAuthenticationStatus({ isAuthenticated: false }));
      return Promise.reject(error);
    }
  );
};

const getSubstate = state => state.profile;

export const hasTriedToAuthSelector = state =>
  getSubstate(state).isAuthenticated !== undefined;

export const isAuthenticatedSelector = state =>
  hasTriedToAuthSelector(state) ? getSubstate(state).isAuthenticated : false;

export const userDisplayNameSelector = state => getSubstate(state).displayName;

export default profile;
