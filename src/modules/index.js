import { combineReducers } from 'redux';
import profile from './profile';
import resources from './resources';

const rootReducer = combineReducers({ profile, resources });

export default rootReducer;
